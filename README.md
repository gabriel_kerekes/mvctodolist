# MVCTodoList

Vašou úlohou je naprogramovať webovú aplikáciu podobnú aplikácii Trello.com.

Ako predlohu môžete využiť projekty, ktoré budú dostupné na bitbuckete:
https://bitbucket.org/gabriel_kerekes/mvctodolist/src/master/ (dostupné to bude v nedeľu (21. 10. 2018) večer)

Aplikácia by mala podporovať:
- viacero používateľov (registrácia/login) - pri vytvárani projektu nastaviť autentifikáciu
- viacero kolekcií TodoListov pre každého používateľa
- viacero TodoListov v každej kolekcii
- viacero TodoListItemov v každom TodoListe
- TodoListItem, TodoList a aj TodoListCollection by mali mať nejaké meno
- pridávanie, odoberanie, zobrazovanie TodoListCollections, TodoListov a aj TodoListItemov

-- jeden TodoList je šedý rámček v predlohe, jeden TodoListItem je jeden biely rámček v TodoListe

V prípade akýchkoľvek otázok píšte na:
gabriel_kerekes@itstep.org

Ako sme sa aj na hodine bavili, ak chcete mať všetky otázky preposielané resp. vedené v jednom spoločnom threade, tak mi to prosím dajte vedieť na tento mail.

Deadline je 28. 11. 2018 (v systéme sa nedá nastaviť vzdialeneší termín)