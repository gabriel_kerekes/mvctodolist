namespace MVCTodoList.Migrations
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using MVCTodoList.Models;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MVCTodoList.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(MVCTodoList.Models.ApplicationDbContext context)
        {
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new ApplicationUserManager(userStore);
            for (var i = 1; i <= 100; i++)
            {
                var username = $"users{i}";

                var user = context.Users.FirstOrDefault(u => u.UserName == username);
                if (user == null)
                {
                    user = new ApplicationUser()
                    {
                        UserName = username,
                        Email = username + "@" + username + "." + username,
                        EmailConfirmed = true,
                        LockoutEnabled = false
                    };

                    var password = username;
                    var result = userManager.CreateAsync(user, password).Result;
                }

                var collection = new TodoListsCollection { ApplicationUser = user, Name = $"Collection {i}", };
                context.TodoListsCollections.Add(collection);

                for (var j = 1; j <= 2; j++)
                {
                    var list = new TodoList { TodoListsCollection = collection, Name = $"List {j}" };
                    context.TodoLists.Add(list);

                    for (var k = 1; k <= 5; k++)
                    {
                        var listItem = new TodoListItem { TodoList = list, Name = $"Item {k}" };
                        context.TodoListItems.Add(listItem);
                    }
                }
            }

            context.SaveChanges();
        }
    }
}
