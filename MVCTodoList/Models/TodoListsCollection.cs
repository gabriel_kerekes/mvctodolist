﻿using System.Collections.Generic;

namespace MVCTodoList.Models
{
    public class TodoListsCollection
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ApplicationUser ApplicationUser { get; set; }
        public List<TodoList> Items { get; set; }

        public TodoListsCollection()
        {
            Items = new List<TodoList>();
        }
    }
}