﻿using System.Collections.Generic;

namespace MVCTodoList.Models
{
    public class TodoList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<TodoListItem> Items { get; set; }

        public TodoListsCollection TodoListsCollection { get; set; }

        public TodoList()
        {
            Items = new List<TodoListItem>();
        }
    }
}