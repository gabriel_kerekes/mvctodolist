﻿namespace MVCTodoList.Models
{
    public class TodoListItem
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public TodoList TodoList {get;set;}
    }
}