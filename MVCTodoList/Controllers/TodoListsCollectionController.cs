﻿using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MVCTodoList.Models;

namespace MVCTodoList.Controllers
{
    [Authorize]
    public class TodoListsCollectionController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();

            var model = db.TodoListsCollections
                .Where(c => c.ApplicationUser.Id == userId)
                .Include(c => c.Items.Select(i => i.Items));

            return View(model);
        }

        public ActionResult Detail(int collectionId)
        {
            var userId = User.Identity.GetUserId();

            var model = db.TodoListsCollections
                .Where(c => c.ApplicationUser.Id == userId && c.Id == collectionId)
                .Include(c => c.Items.Select(i => i.Items))
                .FirstOrDefault();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCollection(string name)
        {
            if (ModelState.IsValid)
            {
                var user = db.Users.Find(User.Identity.GetUserId());

                db.TodoListsCollections.Add(new TodoListsCollection { Name = name, ApplicationUser = user });
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        public ActionResult DeleteCollection(int collectionId)
        {
            var collection = db.TodoListsCollections.Find(collectionId);
            if (collection == null)
            {
                return HttpNotFound();
            }

            db.TodoListsCollections.Remove(collection);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateList(int collectionId, string name)
        {
            if (ModelState.IsValid)
            {
                var collection = db.TodoListsCollections.Find(collectionId);
                db.TodoLists.Add(new TodoList { TodoListsCollection = collection, Name = name });
                db.SaveChanges();

                return RedirectToAction("Detail", new { collectionId });
            }

            return RedirectToAction("Detail", new { collectionId });
        }

        public ActionResult DeleteList(int collectionId, int listId)
        {
            var list = db.TodoLists.Find(listId);
            if (list == null)
            {
                return HttpNotFound();
            }

            db.TodoLists.Remove(list);
            db.SaveChanges();

            return RedirectToAction("Detail", new { collectionId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateItem(int collectionId, int listId, string name)
        {
            var todoList = db.TodoLists.Find(listId);
            if (ModelState.IsValid)
            {
                db.TodoListItems.Add(new TodoListItem { TodoList = todoList, Name = name });
                db.SaveChanges();

                return RedirectToAction("Detail", new { collectionId });
            }

            return RedirectToAction("Detail", new { collectionId });
        }

        public ActionResult DeleteItem(int collectionId, int listId, int itemId)
        {
            var listItem = db.TodoListItems.Find(itemId);
            if (listItem == null)
            {
                return HttpNotFound();
            }

            db.TodoListItems.Remove(listItem);
            db.SaveChanges();

            return RedirectToAction("Detail", new { collectionId });
        }

        #region Async Actions

        public ActionResult CreateCollectionAsync(string name)
        {
            if (ModelState.IsValid)
            {
                var user = db.Users.Find(User.Identity.GetUserId());

                var newCollection = new TodoListsCollection { Name = name, ApplicationUser = user };
                db.TodoListsCollections.Add(newCollection);
                db.SaveChanges();

                return PartialView("_CollectionView", newCollection);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        public ActionResult DeleteCollectionAsync(int collectionId)
        {
            var collection = db.TodoListsCollections.Find(collectionId);
            if (collection == null)
            {
                return HttpNotFound();
            }

            db.TodoListsCollections.Remove(collection);
            db.SaveChanges();

            return new EmptyResult();
        }

        public ActionResult CreateListAsync(int collectionId, string name)
        {
            if (ModelState.IsValid)
            {
                var todoListCollection = db.TodoListsCollections.Find(collectionId);
                var newList = new TodoList { TodoListsCollection = todoListCollection, Name = name };
                db.TodoLists.Add(newList);
                db.SaveChanges();

                return PartialView("_ListView", newList);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        public ActionResult DeleteListAsync(int collectionId, int listId)
        {
            var list = db.TodoLists.Find(listId);
            if (list == null)
            {
                return HttpNotFound();
            }

            db.TodoLists.Remove(list);
            db.SaveChanges();

            return new EmptyResult();
        }
        
        public ActionResult CreateItemAsync(int collectionId, int listId, string name)
        {
            if (ModelState.IsValid)
            {
                var todoList = db.TodoLists.Find(listId);
                var newItem = new TodoListItem { TodoList = todoList, Name = name };
                db.TodoListItems.Add(newItem);
                db.SaveChanges();

                return PartialView("_ItemView", newItem);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        public ActionResult DeleteItemAsync(int collectionId, int listId, int itemId)
        {
            var listItem = db.TodoListItems.Find(itemId);
            if (listItem == null)
            {
                return HttpNotFound();
            }

            db.TodoListItems.Remove(listItem);
            db.SaveChanges();

            return new EmptyResult();
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
