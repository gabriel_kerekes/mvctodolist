﻿using System.Web.Mvc;
using System.Web.Routing;

namespace MVCTodoList
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            
            routes.MapRoute(
                name: "TodoListsCollection",
                url: "{controller}/{action}/{collectionId}/{listId}/{itemId}",
                defaults: new { controller = "TodoListsCollection", action = "Index", collectionId = UrlParameter.Optional, listId = UrlParameter.Optional, itemId = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
